# Maps 3D Mesh Cleanup

This is a Blender script intended to streamline the process of cleaning up data captured using Renderdoc and MapsModelImporter.  It is recommended that this script is used with Blender 3.6 or later because of improvements to the UV unwrapping and packing performance in these later versions.

# Changelog

2023.05.11 - Version 0.36 - Simplified script to leverage Blender 3.6 new UV unwrapping and packing performance. The script no longer needs to manually create the UV maps. 

2023.03.15 - Version 0.2 - Improved mesh joining method and reporting method for situations with many meshes.

## Getting Started

### Prerequisites

Blender 3.6.0 or later is recommended.
[Download here.](https://builder.blender.org/download/daily/)

A collection with multiple objects to be combined into one.

### Usage

1) Copy the contents of mesh_cleanup.py and paste into the Blender text editor.

2) Toggle settings at the top of the script

3) Open the Blender terminal window to see printouts of script progress

4) Run the script.
